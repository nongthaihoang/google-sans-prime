### 2025-01-16
- Pixel: Fixed the `GSOPSZ` option did not work.

#### _— Previous Version —_

#### 2025-01-14 (Major update)
- Updated Google Sans font.
- Updated Config: Removed the `ROUNDED` option. Add `ROND 50` to Font Styles instead.  
  E.g. `UR = ... ROND 50`
- Updated OMF template.

#### 2025-01-07
- Updated OMF template:  
  When UPDATING, the installation will be ABORTED if it can NOT get the original fontxml to prevent bootloop/problems.  
  In this case, please remove the module first then install it as usual.

#### 2024-12-28
- Updated OMF template: Allowed updating the module (no need to remove it first).

#### 2024-12-26
- Updated OMF template to support Android 15
- Updated config: added font preview links and axes ranges. Your config will be RESET!

[Full Changelog](https://gitlab.com/nongthaihoang/google-sans-prime/-/commits/master)
