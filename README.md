# Google San Prime
**[Telegram](https://t.me/ohmyfont_channel) —
[Download](https://gitlab.com/nongthaihoang/google-sans-prime/-/raw/master/releases/GSP.zip) —
[Changelog](https://gitlab.com/nongthaihoang/google-sans-prime/-/blob/master/CHANGELOG.md)**

## Description
GSP is a Magisk module (based on [OMF](https://gitlab.com/nongthaihoang/omftemplate)) that replaces the default Android system font (i.e. Roboto) with Google Sans (Android 12).  

## Fonts
#### [GS](https://nongthaihoang.gitlab.io/omf-demo/gsvf.html)
> This **sans-serif** typeface is the system UI font for Google Pixel Android and is the default font for Google apps. GS features four weights, variable optical sizes for headline and body text. GS Prime is a modified version of GS, includes a [rounded corners](https://nongthaihoang.gitlab.io/omf-demo/gsvfr.html) variant, optional fixes and useful features. GS supports many languages across Latin, Greek, and Cyrillic scripts.
#### [GS Mono](https://nongthaihoang.gitlab.io/omf-demo/gsm.html)
> This **monospaced** variant of GS enables alignment between rows and columns of text, and is used in coding environments like code editor, terminal apps. GS Mono features four weights.

## Useful Links
- [Introducing variable fonts](https://fonts.google.com/knowledge/introducing_type/introducing_variable_fonts)
- [Afdko](https://gitlab.com/nongthaihoang/oh_my_font#afdko--source) extension.

## Show Your Gratitude
- [PayPal](https://paypal.me/nongthaihoang)
