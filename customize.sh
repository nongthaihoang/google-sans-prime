. ${SH:=$MODPATH/ohmyfont}

### INSTALLATION ###

# Text filter
fontcust() {
    [ $f = $SER ] && case $* in *ROND\ [1-9]*) f=$SRM ;; esac
    [ $fa = $SA -a $f = $ORISS ] && case $* in *ROND\ [1-9]*) f=temp ;; esac
}

ui_print ' _____ _____ _____     _           '
ui_print '|   __|   __|  _  |___|_|_____ ___ '
ui_print '|  |  |__   |   __|  _| |     | -_|'
ui_print '|_____|_____|__|  |_| |_|_|_|_|___|'
ui_print '                                   '

ui_print '- Installing'

ui_print '+ Prepare'
prep

ui_print '+ Configure'
config
# if opsz 17, switch to GS
for i in $FW; do i=`up $i`
    eval S$i=\$U$i
    eval O$i=\$U$i
    eval "case \"\$U$i\" in *ROND\ [1-9]*) ROUNDED=true;; esac"
    eval "case \"\$U$i\" in *opsz\ 17.*);; *opsz\ 17*) SANS=$SE;; esac"
done

ui_print '+ Google Sans'
[ $SANS = $SE ] || ui_print '  Flex'
${ROUNDED:=false} && ui_print '  Rounded'
install_font
cpf $SER $SERI $SRM
$MONO && ui_print '  Mono'
# apply OTL to Rounded
([ "${OTL:=`valof OTL`}" ] && afdko && {
    SS=$SRM SSI=
    otl
}) &>$Null

src

ui_print '+ Rom'
# keep GS default opsz
[ "${GSOPSZ:=`valof GSOPSZ`}" = false ] || {
    for i in $FW; do i=`up $i`
        eval U$i=\"`eval echo $"U$i" | sed 's|opsz[ [:digit:].]*||'`\"
        eval S$i=\$U$i
        eval O$i=\$U$i
    done
    axis_del=false
}
rom
# reinstall Text when Flex is used
[ $SANS != $SE ] && ! $GSOPSZ && (
    fa=$Gs-text.* up=$SER it=$SERI
    fontinst
)

ui_print '- Finalizing'
# make Flex fallback to GS
[ $SS = $ORISS ] && {
    fallback $SA
    xml "$SAF{s|temp|$SS|}"
    xml "/$FA >/,$FAE{/$N/{s|$SS|$SER|;s|temp|$SRM|};/$I/s|$SS|$SERI|;s|17.\"|18\"|}"
}
fontspoof

svc
finish
